package cl.toeska

import groovy.util.logging.Slf4j

import org.apache.tika.metadata.Metadata
import org.apache.tika.parser.ParseContext
import org.apache.tika.parser.html.HtmlParser
import org.apache.tika.sax.BodyContentHandler

/**
 * HTML to plain text
 * 
 * @author Jorge Riquelme Santana
 * @date 13-06-2013
 *
 */
@Slf4j
class TextExtractor {
	def toText(File html, File txt) {
		def input = new FileInputStream(html)
		def handler = new BodyContentHandler()
		def metadata = new Metadata()
		new HtmlParser().parse(input, handler, metadata, new ParseContext())
		def text = handler.toString()
		
		def w = txt.newWriter("UTF-8")
		text.eachLine { l ->
			def line = l.trim()
			if (line.length() > 0 && !(line ==~ /[\s]+/)
				&& !line.contains("##LOC[")) {
				w << line << System.getProperty("line.separator")
			}
		}
		w.close()
	}
}
