package cl.toeska

import geb.Page

/**
 * mercadopublico.cl page
 * 
 * @author Jorge Riquelme Santana
 * @date 13-06-2013
 *
 */
class MercadoPublicoPage extends Page {
	static url = "https://www.mercadopublico.cl/Portal/login.aspx"
	static content = {
		searchField() { $("input", type: "text", id: "txt_SearchFast") }
		searchButton() { $("input", type: "submit", id: "btn_enviar") }
		resultsDiv(wait: true) { $("div", id: "resultados") }
		resultLink(wait: true, required: false) { $("a", class: "link-lic-ficha-nombre") }
	}
}
