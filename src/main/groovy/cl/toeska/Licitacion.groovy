package cl.toeska

import java.io.File;

/**
 * Licitacion
 * 
 * @author Jorge Riquelme Santana
 * @date 16-06-2013
 *
 */
class Licitacion {
	String code
	String url
	File engTxtFile
	File spaHtmlFile
	File spaTxtFile
	
	def init(File outputDirectory) {
		engTxtFile = new File(outputDirectory, "$code-eng.txt")
		spaHtmlFile = new File(outputDirectory, "$code-spa.html")
		spaTxtFile = new File(outputDirectory, "$code-spa.txt")
	}
}
