package cl.toeska

import groovy.util.logging.Slf4j
import groovyx.gpars.*

/**
 * Main
 * 
 * @author Jorge Riquelme Santana
 * @date 13-06-2013
 *
 */
@Slf4j
class LicitacionesExtractor {
	def config
	
	public static void main(String[] args) {
		def cli = new CliBuilder()
		cli.with {
			usage: "run.sh"
			i longOpt: "input", "input file", args: 1, argName: "IFILE", required: true
			o longOpt: "output", "output directory", args: 1, argName: "ODIR", required: true
			t longOpt: "translate", "translate documents to english"
			f longOpt: "firefox", "use firefox for web scrapping"
		}
		def opt = cli.parse(args)
		if (opt == null) {
			return
		}
		else if(args.length == 0) {
			cli.usage()
			return
		}

		def input = new File(opt.i)
		def output = new File(opt.o)
		if (!input.exists() || !input.isFile()) {
			println "${opt.i} doesn't exists or itsn't a regular file"
			System.exit(1);
		}
		else if (!output.exists() || !output.isDirectory()) {
			println "${opt.o} doesn't exists or itsn't a directory"
			System.exit(2);
		}
		else {
			def extractor = new LicitacionesExtractor()
			extractor.run(input, output, opt.t, opt.f)
		}
	}
	
	LicitacionesExtractor() {
		config = new ConfigSlurper().parse(new File("config.groovy").toURI().toURL())
	}

	def run(File input, File output, boolean translate, boolean firefox) {
		def fetcher = new Fetcher()
		def textExtractor = new TextExtractor()
		def translator = new Translator(key: config.google.key)

		fetcher.init(config.phantomjs, firefox)
		
		input.eachLine { code ->
			def lic = new Licitacion(code: code)
			lic.init(output)

			try {
				//get spa html and save it
				fetcher.fetch(lic)
				
				//get plain text (in spanish) and save it
				textExtractor.toText(lic.spaHtmlFile, lic.spaTxtFile)
				
				//if translation is active, translate page to english and save it
				if (translate) {
					translator.translate(lic)
				}
			} catch(ex) {
				log.error ("error:", ex)
			}
		}

		fetcher.shutdown()
	}
}

