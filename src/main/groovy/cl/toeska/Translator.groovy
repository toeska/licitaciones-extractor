package cl.toeska;

import com.google.api.GoogleAPI;
import com.google.api.translate.Language;
import com.google.api.translate.Translate;

import groovy.util.logging.Slf4j
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method

/**
 * Spanish to english translator
 * 
 * @author Jorge Riquelme Santana
 * @date 13-06-2013
 *
 */
@Slf4j
public class Translator {
	String key
	
	def translate(Licitacion lic) {
		GoogleAPI.setHttpReferrer(lic.url)
		GoogleAPI.setKey(key)
		
		def w = lic.engTxtFile.newWriter("UTF-8")
		lic.spaTxtFile.getText().eachLine { l ->
			w << Translate.DEFAULT.execute(l, Language.SPANISH, Language.ENGLISH) << System.getProperty("line.separator")
		}
		w.close()
	}
}
