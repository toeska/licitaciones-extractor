package cl.toeska

import geb.Browser
import groovy.util.logging.Slf4j

import org.openqa.selenium.Capabilities
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.phantomjs.PhantomJSDriver
import org.openqa.selenium.phantomjs.PhantomJSDriverService
import org.openqa.selenium.remote.DesiredCapabilities

/**
 * Licitacion fetcher
 * 
 * @author Jorge Riquelme Santana
 * @date 13-06-2013
 *
 */
@Slf4j
class Fetcher {
	def browser
	
	def init(String phantomjsBinary, boolean firefox) {
		browser = new Browser()
		
		if (firefox) {
			browser.driver = new FirefoxDriver()
		} else {
			//phantojs driver
			Capabilities caps = new DesiredCapabilities()
			caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, phantomjsBinary)
			browser.driver = new PhantomJSDriver(caps)
		}
	}
	
	def fetch(Licitacion lic) {
		log.debug "fetching $lic.code"

		browser.to MercadoPublicoPage

		browser.searchField.value(lic.code)
		browser.searchButton.click()

		lic.url = browser.resultLink.@href

		log.debug "licitacion url for code $lic.code: $lic.url"

		if (lic.url != null) {
			def fos = new FileOutputStream(lic.spaHtmlFile)
			def out = new BufferedOutputStream(fos)
			out << new URL(lic.url).openStream()
			out.close()
		} else {
			throw new RuntimeException("licitacion $lic.code not found")
		}
	}

	def shutdown() {
		browser.quit()
	}
}
