package cl.toeska

import groovyx.net.http.Method
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.RESTClient
import org.junit.Test;

/**
 * Google translator API test
 * 
 * @author Jorge Riquelme Santana
 * @date 14-06-2013
 *
 */
class GoogleTransalatorApiTest extends GroovyTestCase {
	@Test
	void testApi() {
		def key = "AIzaSyBfrd0IxnKOwA9T-uYaT1KGQYAuoivmpe4"
		def url = "https://www.googleapis.com/language/translate/v2?key=$key&source=es&target=en"
		
		def translatorApi = new HTTPBuilder(url)
		translatorApi.request(Method.POST, ContentType.JSON) {
			headers."X-HTTP-Method-Override" = "GET"
			send ContentType.URLENC, [q: "hello world!"]
			
			response.success = { resp, json ->
				println "text ${json.data.translations[0].translatedText}"
			}
			
			response.failure = { resp, json ->
				log.error "error: ${resp.statusLine}"
			}
		}
	}
}
