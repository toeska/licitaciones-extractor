package cl.toeska

import org.junit.Test;

import groovy.util.logging.Slf4j;

/**
 * Fetcher test
 * 
 * @author Jorge Riquelme Santana
 * @date 13-06-2013
 *
 */
class FetcherTest extends GroovyTestCase {
	@Test
	void testFetcher() {
		def lic = new Licitacion(code: "5147-18-LP13")
		lic.init(new File(System.getProperty("java.io.tmpdir")))

		def fetcher = new Fetcher()
		fetcher.fetch(lic)
		
		assert lic.url == "http://www.mercadopublico.cl/Procurement/Modules/RFB/DetailsAcquisition.aspx?qs=zzIgX6Z4QbOnULvnJAG6Hc2Qbuuq9kw0lVInnTZZVKdMosHQxOfgaDmIknGzM5Gh"
	}
}
