package cl.toeska

import org.junit.Test;

/**
 * Translator test
 * 
 * @author Jorge Riquelme Santana
 * @date 14-06-2013
 *
 */
class TranslatorTest extends GroovyTestCase {
	//run with -Dgoogle-key=<your google api key>
	@Test
	void testTranslation() {
		def lic = new Licitacion(code: "5147-18-LP13", url: "http://www.mercadopublico.cl/Procurement/Modules/RFB/DetailsAcquisition.aspx?qs=zzIgX6Z4QbOnULvnJAG6Hc2Qbuuq9kw0lVInnTZZVKdMosHQxOfgaDmIknGzM5Gh")
		lic.init(new File(System.getProperty("java.io.tmpdir")))
		lic.spaTxtFile = new File("src/test/resources/5147-18-LP13-spa.txt")
		def translator = new Translator(key: System.getProperty("google-key"))
		translator.translate(lic)
	}
}
