package cl.toeska;

import org.junit.Test

/**
 * {@link TextExtractor} tests
 * 
 * @author Jorge Riquelme Santana
 * @date 13-06-2013
 * 
 */
public class TextExtractorTest extends GroovyTestCase {
	@Test
	void testExtractor() {
		def extractor = new TextExtractor()
		def from = new File("src/test/resources/5147-18-LP13-spa.html")
		def control = new File("src/test/resources/5147-18-LP13-spa.txt")
		def to = File.createTempFile("5147-18-LP13", ".txt")
		extractor.toText(from, to)
		assert to.getText() == control.getText()
	}
}
